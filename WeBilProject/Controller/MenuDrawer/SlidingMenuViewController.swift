//
//  SlidingMenuViewController.swift
//  WeBilProject
//
//  Created by Kosign Class 01 on 12/29/20.
//

import UIKit
import NavigationDrawer

class SlidingMenuViewController: UIViewController {
    
    @IBOutlet weak var tableMenu: UITableView!
    var interactor:Interactor? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableMenu.register(EventTableViewCell.nib(), forCellReuseIdentifier: EventTableViewCell.identifierCell)
        tableMenu.register(ProfileTableViewCell.nib(), forCellReuseIdentifier: ProfileTableViewCell.identifier)
        tableMenu.register(SettingTableViewCell.nib(), forCellReuseIdentifier: SettingTableViewCell.identifierCell)
        tableMenu.register(AboutUsTableViewCell.nib(), forCellReuseIdentifier: AboutUsTableViewCell.identifierCell)
        tableMenu.register(LogoutTableViewCell.nib(), forCellReuseIdentifier: LogoutTableViewCell.identifierCell)
    }
    
    //Handle Gesture
    @IBAction func handleGesture(sender: UIPanGestureRecognizer) {
        let translation = sender.translation(in: view)
        
        let progress = MenuHelper.calculateProgress(translationInView: translation, viewBounds: view.bounds, direction: .Left)
        
        MenuHelper.mapGestureStateToInteractor(
            gestureState: sender.state,
            progress: progress,
            interactor: interactor){
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func closeBtnPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
extension SlidingMenuViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellEvent = tableView.dequeueReusableCell(withIdentifier: EventTableViewCell.identifierCell, for: indexPath)
        let cellProfile = tableView.dequeueReusableCell(withIdentifier: ProfileTableViewCell.identifier, for: indexPath)
        let cellSetting = tableView.dequeueReusableCell(withIdentifier: SettingTableViewCell.identifierCell, for: indexPath)
        let cellAboutUs = tableView.dequeueReusableCell(withIdentifier: AboutUsTableViewCell.identifierCell, for: indexPath)
        let cellLogout = tableView.dequeueReusableCell(withIdentifier: LogoutTableViewCell.identifierCell, for: indexPath)
        if indexPath.row == 0 {
            return cellEvent
        }else if indexPath.row == 1{
            return cellProfile
        }else if indexPath.row == 2{
            return cellSetting
        }else if indexPath.row == 3{
            return cellAboutUs
        }else{
            return cellLogout
        }
    }
    
    
}
