//
//  MenuDrawerViewController.swift
//  WeBilProject
//
//  Created by Kosign Class 01 on 12/29/20.
//

import UIKit
import NavigationDrawer

class MenuDrawerViewController: UIViewController {
    
    //1.
    let interactor = Interactor()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //2.
//    @IBAction func homeButtonPressed(_ sender: UIBarButtonItem) {
//        performSegue(withIdentifier: "showSlidingMenu", sender: nil)
//    }
//    
//    //3. Add a Pan Gesture to slide the menu from Certain Direction
//    @IBAction func edgePanGesture(sender: UIScreenEdgePanGestureRecognizer) {
//        let translation = sender.translation(in: view)
//        
//        let progress = MenuHelper.calculateProgress(translationInView: translation, viewBounds: view.bounds, direction: .Right)
//        
//        MenuHelper.mapGestureStateToInteractor(
//            gestureState: sender.state,
//            progress: progress,
//            interactor: interactor){
//            self.performSegue(withIdentifier: "showSlidingMenu", sender: nil)
//        }
//    }
    
    //4. Prepare for segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationViewController = segue.destination as? SlidingMenuViewController {
            destinationViewController.transitioningDelegate = self
            destinationViewController.interactor = self.interactor
        }
    }
    
}

extension MenuDrawerViewController: UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return PresentMenuAnimator(direction: .Left)
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return DismissMenuAnimator()
    }
    
    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return interactor.hasStarted ? interactor : nil
    }
    
    func interactionControllerForPresentation(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return interactor.hasStarted ? interactor : nil
    }
}
