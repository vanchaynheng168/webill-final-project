//
//  BillViewController.swift
//  WeBilProject
//
//  Created by Nheng Vanchhay 01 on 12/29/20.
//

import UIKit

class BillViewController: UIViewController {
    
    @IBOutlet weak var billSegement: UISegmentedControl!
    @IBOutlet weak var displayScreen: UIView!
    let billHistory = BillHistoryViewController()
    let billGenerate = BillGenerateViewController()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //change color title segement
        UISegmentedControl.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .selected)
        
        setup()
    }
    func setup(){
        
        self.displayScreen.addSubview(billGenerate.view)
        self.displayScreen.addSubview(billHistory.view)
        
        billHistory.view.frame = self.displayScreen.bounds
        billGenerate.view.frame = self.displayScreen.bounds
        
    }
    @IBAction func didTabSegement(segement: UISegmentedControl){
        
        billHistory.view.isHidden = true
        billGenerate.view.isHidden = true
        if segement.selectedSegmentIndex == 0 {
            billHistory.view.isHidden = false
        }else{
            billGenerate.view.isHidden = false
        }
        
    }
    
}
