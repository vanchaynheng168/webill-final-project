//
//  UpdateProfileViewController.swift
//  WeBilProject
//
//  Created by Nheng vanchhay 01 on 1/4/21.
//

import UIKit

class UpdateProfileViewController: UIViewController {

    @IBOutlet weak var tableUpdateProfile: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
}
extension UpdateProfileViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cellProfile = tableView.dequeueReusableCell(withIdentifier: "profileCell", for: indexPath)
        let cellInputText = tableView.dequeueReusableCell(withIdentifier: "inputTextCell", for: indexPath)
        let cellSelectGender = tableView.dequeueReusableCell(withIdentifier: "selectGenderCell", for: indexPath)
        let cellButtonSave = tableView.dequeueReusableCell(withIdentifier: "buttonSaveCell", for: indexPath)
       
        if indexPath.row == 0 {
            return cellProfile
        }else if indexPath.row == 1{
            return cellInputText
        }else if indexPath.row == 2{
            return cellSelectGender
        }else{
            return cellButtonSave
        }
        
    }
    
    
}
