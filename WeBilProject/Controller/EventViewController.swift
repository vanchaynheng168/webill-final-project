//
//  EventViewController.swift
//  WeBilProject
//
//  Created by Nheng Vanchhay 01 on 12/28/20.
//

import UIKit

class EventViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tableEvent: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableEvent.dataSource = self
        tableEvent.delegate = self
        // Do any additional setup after loading the view.
        tableEvent.register(CardEventTableViewCell.nib(), forCellReuseIdentifier: CardEventTableViewCell.identifireCell)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CardEventTableViewCell.identifireCell, for: indexPath)
        return cell
    }
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        500
//    }
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableView.automaticDimension
//    }
}
