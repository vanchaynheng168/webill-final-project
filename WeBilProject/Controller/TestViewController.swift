//
//  TestViewController.swift
//  WeBilProject
//
//  Created by Kosign Class 01 on 12/24/20.
//

import UIKit

class TestViewController: UIViewController {

    @IBOutlet weak var radioSelected: UIButton!
    @IBOutlet weak var radioUnselect: UIButton!
    @IBOutlet weak var password: customUITextField!
    @IBOutlet weak var email: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        password.isSecureTextEntry = true
        
    }
    

    @IBAction func radioSelected(_ sender: Any) {
        
        radioSelected.setBackgroundImage(UIImage(named: "radioSelected"), for: .normal)
        radioUnselect.setBackgroundImage(UIImage(named: "radio"), for: .normal)
       
    }
    @IBAction func radioUnSelect(_ sender: Any) {
        radioUnselect.setBackgroundImage(UIImage(named: "radioSelected"), for: .normal)
        radioSelected.setBackgroundImage(UIImage(named: "radio"), for: .normal)
    }
}
