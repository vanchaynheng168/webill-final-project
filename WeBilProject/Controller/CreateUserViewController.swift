//
//  CreateUserViewController.swift
//  WeBilProject
//
//  Created by Kosign Class 01 on 12/28/20.
//

import UIKit

class CreateUserViewController: UIViewController {

    @IBOutlet weak var female: UIButton!
    @IBOutlet weak var male: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func pressMale(_ sender: Any) {
        female.backgroundColor = .white
        male.backgroundColor = .red
        
    }
    @IBAction func pressFemale(_ sender: Any) {
        male.backgroundColor = .white
        female.backgroundColor = .red
    }
}
