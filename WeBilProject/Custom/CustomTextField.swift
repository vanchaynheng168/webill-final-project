//
//  CustomeTextField.swift
//  WeBilProject
//
//  Created by Kosign Class 01 on 12/30/20.
//

import Foundation
import UIKit

@IBDesignable
open class customUITextField: UITextField {
    
    func setup() {
        let border = CALayer()
        let width = CGFloat(2.0)
        border.borderColor = UIColor.darkGray.cgColor
        border.frame = CGRect(x: 0, y: frame.size.height - width, width: bounds.width+1000, height: frame.size.height)
        borderStyle = .none
        border.borderWidth = width
        border.borderColor = #colorLiteral(red: 0.0424226746, green: 0.4586912394, blue: 0.6748422384, alpha: 1)
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
}
