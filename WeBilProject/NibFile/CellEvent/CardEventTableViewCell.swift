//
//  CardEventTableViewCell.swift
//  WeBilProject
//
//  Created by Nheng Vanchhay 01 on 12/28/20.
//

import UIKit

class CardEventTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    static let identifireCell: String = "cardEvent"

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    static func nib() -> UINib {
        return UINib(nibName: "CardEventTableViewCell", bundle: nil)
    }
    
}
