//
//  BillHistoryViewController.swift
//  WeBilProject
//
//  Created by Kosign Class 01 on 12/29/20.
//

import UIKit

class BillHistoryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableHistoryBill: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableHistoryBill.delegate = self
        tableHistoryBill.dataSource = self
        
        tableHistoryBill.register(BillHistoryTableViewCell.nib(), forCellReuseIdentifier: BillHistoryTableViewCell.identifireCell)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: BillHistoryTableViewCell.identifireCell, for: indexPath) as! BillHistoryTableViewCell
        return cell
    }
    

}
