//
//  BillHistoryTableViewCell.swift
//  WeBilProject
//
//  Created by Kosign Class 01 on 12/29/20.
//

import UIKit

class BillHistoryTableViewCell: UITableViewCell {

    static let identifireCell: String = "billHistoryCell"
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    static func nib() -> UINib{
        return UINib(nibName: "BillHistoryTableViewCell", bundle: nil)
    }
    
}
