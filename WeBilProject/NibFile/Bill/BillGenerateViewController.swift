//
//  BillGenerateViewController.swift
//  WeBilProject
//
//  Created by Kosign Class 01 on 12/29/20.
//

import UIKit

class BillGenerateViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

}
extension BillGenerateViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellImage = tableView.dequeueReusableCell(withIdentifier: "imageCell", for: indexPath)
        let cellInputText = tableView.dequeueReusableCell(withIdentifier: "inputTextCell", for: indexPath)
        let cellButton = tableView.dequeueReusableCell(withIdentifier: "buttonCell", for: indexPath)
        if indexPath.row == 0 {
            return cellImage
        }else if indexPath.row == 1 {
            return cellInputText
        }else {
            return cellButton
        }
    }
    
    
}
