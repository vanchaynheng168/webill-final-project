//
//  ProfileTableViewCell.swift
//  WeBilProject
//
//  Created by Kosign Class 01 on 12/31/20.
//

import UIKit

class ProfileTableViewCell: UITableViewCell {

    static let identifier: String = "cellProfile"
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    static func nib() -> UINib{
        return UINib(nibName: "ProfileTableViewCell", bundle: nil)
    }
    
}
